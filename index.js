'use strict';
/* Learning From natural npm module based on NLP 
   Now On summarize the below code is based on following stuff 
   1.Stemming of words (using Porterstemmer and Lancasterstemmer Algorithm)
   
   2.Phonetics of words 
   (checking similarity between two words based on the pronunciation using Soundex , Metaphone , DoubleMetaphone Algorithm)

   3.BayesClassifier(classification of the inputs based on the training data)
*/

let natural = require('natural');
let stemmer = natural.PorterStemmer;
let stem = stemmer.stem('stems');
console.log(stem);
stem = stemmer.stem('stemming');
console.log(stem);
stem = stemmer.stem('stemmed');
console.log(stem);
stem = stemmer.stem('stem');
console.log(stem);
stemmer.attach();
stem = 'stemmings'.stem();
console.log(stem);
var stems = 'stems returning idiotism'.tokenizeAndStem();
console.log(stems);classifier.save('classifier.json', function(err, classifier) {
    console.log('classifier got saved into classifier.json...');
});
// upon passing true as parameter it won't stem the Stop words like 'I' but upon empty argument it will 
var stems = 'istems returning idiotism .'.tokenizeAndStem(true);
console.log(stems);
var stemmer1 = natural.LancasterStemmer;
console.log(stemmer1.stem('logged procedures'))
console.log(stemmer.stem('logged procedures'))

let phonetic = natural.Metaphone;
var a = 'bucks';
var b='money';
var c = 'good morning'
var d = 'guten morgen'
// it compares the pronounciation of both the words and sattisfies the if condition 
if(phonetic.compare(a,b)){
    console.log('both sound alike');
}
console.log(phonetic.process(a)); // it will print the pronounciation stuff
console.log(phonetic.process(b));
phonetic.attach(); // it enables to access the phonetic functions directly against the strings
console.log('I phonetics rock'.tokenizeAndPhoneticize(true));

var phonetic1 = natural.SoundEx;
console.log(phonetic1.compare(a,b));
console.log(phonetic1.process(a))
console.log(phonetic1.process(b))
var phonetic2 = natural.DoubleMetaphone;
console.log('comparision is ',phonetic2.compare(c,d));
console.log('process1 as ',phonetic2.process(c))
console.log('process2 as ',phonetic2.process(d))

// classifier stuff 
// Bayes Naive Classifier 

// basic example of spam filter based on the training data
var classifier = new natural.BayesClassifier(stemmer1);
// providing the training data to the classifier
// classifier.addDocument('iam a good boy','good');
// classifier.addDocument('iam a bad boy','bad');
// classifier.addDocument('beautiful stuff','good');
// classifier.addDocument('ugly stuff','bad');
// classifier.addDocument('pleasant','good');
// classifier.addDocument('horrible','bad');
// classifier.addDocument('pass','good');
// classifier.addDocument('fail','bad');
// classifier.addDocument('iam a grt guy','good');
// classifier.addDocument('iam an unworthy guy','bad');

classifier.addDocument("my unit-tests failed.", 'software');
classifier.addDocument("tried the program, but it was buggy.", 'software');
classifier.addDocument("the drive has a 2TB capacity.", 'hardware');
classifier.addDocument("i need a new power supply.", 'hardware');

//training itself
classifier.train();

// now checking based on the input we classify it and provide a label.
// console.log(classifier.classify('pleasant weather')); // good
// console.log(classifier.classify('grt carry on')); // good
// console.log(classifier.classify('horrible state')); // bad

// By defaut stemming of words happen with porter stemmer algorithm 
// but we can opt for different LancasterStemmer algorithm 
// by passing instance of LancasterStemmer as argument in BayesClassifier constructor

// console.log(classifier.classify('unworthy stuff')); // bad
// console.log(classifier.classify('good or bad please do it')); // good

// saving the classifier info into the localdisk 
classifier.save('classifier.json', function(err, classifier) {
    console.log('classifier got saved into classifier.json...');
});
// loading the classifier file and classifying the input stuff
natural.BayesClassifier.load('classifier.json',null,(err,classifier)=>{
    console.log(classifier.classify('did the test pass?'));
})


